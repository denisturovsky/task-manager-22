package ru.tsc.denisturovsky.tm.api.repository;

import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
