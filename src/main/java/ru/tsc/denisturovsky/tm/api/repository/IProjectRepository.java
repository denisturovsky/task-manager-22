package ru.tsc.denisturovsky.tm.api.repository;

import ru.tsc.denisturovsky.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

}
