package ru.tsc.denisturovsky.tm.api.service;

public interface IServiceLocator {

    ILoggerService getLoggerService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

}
