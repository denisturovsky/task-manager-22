package ru.tsc.denisturovsky.tm.api.repository;

import ru.tsc.denisturovsky.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    M remove(String userId, M model);

    M removeOneById(String userId, String id);

    M removeOneByIndex(String userId, Integer index);

    M add(String userId, M model);

    void clear(String userId);

    int getSize(String userId);

}
