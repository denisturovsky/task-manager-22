package ru.tsc.denisturovsky.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Command ``" + argument + "`` not supported...");
    }

}
