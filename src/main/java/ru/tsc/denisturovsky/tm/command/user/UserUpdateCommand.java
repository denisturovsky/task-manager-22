package ru.tsc.denisturovsky.tm.command.user;

import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

public final class UserUpdateCommand extends AbstractUserCommand {

    public static final String NAME = "user-update";

    public static final String DESCRIPTION = "Update user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        String userId = getAuthService().getUserId();
        System.out.println("[ENTER FIRST NAME:]");
        String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        String middleName = TerminalUtil.nextLine();
        getUserService().userUpdate(userId, firstName, lastName, middleName);
    }

}
