package ru.tsc.denisturovsky.tm.command.project;

import ru.tsc.denisturovsky.tm.api.service.IProjectService;
import ru.tsc.denisturovsky.tm.api.service.IProjectTaskService;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

}
